FROM ubuntu:bionic

#ENVIRONNEMENT
ENV GLIB_PACKAGE_BASE_URL https://github.com/sgerrand/alpine-pkg-glibc/releases/download
ENV GLIB_VERSION 2.31-r0

ENV JAVA_HOME /usr/lib/jvm/java-1.8.0-openjdk-amd64

ENV GRADLE_HOME /usr/local/gradle
ENV GRADLE_VERSION 4.10.3

ENV ANDROID_SDK_ROOT /usr/local/android-sdk-linux

ENV PATH ${GRADLE_HOME}/bin:${JAVA_HOME}/bin:${ANDROID_SDK_ROOT}/tools/bin:$ANDROID_SDK_ROOT/platform-tools:$PATH

# INSTALL JAVA
RUN apt update
RUN apt install openjdk-8-jdk curl unzip checkinstall build-essential autoconf automake -y
RUN apt install git -y

#INSTALL Graddle
RUN mkdir -p ${GRADLE_HOME}
RUN curl -L https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-all.zip > /tmp/gradle.zip
RUN unzip /tmp/gradle.zip -d ${GRADLE_HOME}
RUN mv ${GRADLE_HOME}/gradle-${GRADLE_VERSION}/* ${GRADLE_HOME}
RUN rm -r ${GRADLE_HOME}/gradle-${GRADLE_VERSION}/

# INSTALL COMMAND LINE TOOLS
RUN mkdir -p ${ANDROID_SDK_ROOT}
RUN curl -L https://dl.google.com/android/repository/commandlinetools-linux-6200805_latest.zip > /tmp/tools.zip
RUN unzip /tmp/tools.zip -d ${ANDROID_SDK_ROOT}

# INSTALL NODE
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install -y nodejs
RUN npm i -g npm@latest

RUN npm -v
RUN node -v

# INSTALL IONIC
RUN npm install -g ionic

# ACCEPT LICENSES
RUN yes | sdkmanager --sdk_root=${ANDROID_SDK_ROOT} --licenses

# BUILD TOOLS INSTALL
RUN sdkmanager --sdk_root=${ANDROID_SDK_ROOT} --install "build-tools;29.0.3"

#FILES DELETION
RUN rm -rf /tmp/* /var/cache/apk/*